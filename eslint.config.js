module.exports = { 
    "extends": ["airbnb", "prettier"],
    "plugins": ["prettier/recommended"],
    "rules": {
        "prettier/prettier": ["error"]
    }
};