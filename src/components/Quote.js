import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

class Quote extends Component {

    constructor(props){
        super(props);

        this.state = {
            quote: {}
        };
    }

    componentWillMount(){
        this.loadQuotes();        
    }

    async loadQuotes(){      
        const {symbol} = this.props;     
        const response = await axios.get(`https://api.iextrading.com/1.0/stock/${symbol}/quote`);
        this.setState ( { quote: response.data });
    }

    render() {
        const { quote } = this.state;
        return ( 
            <div className="stock-box">        
                <div className="stock-header">                
                    <h3>{quote.companyName} ({quote.symbol})</h3> 
                </div>

                <div className="stock-detail">
                <div className="row">

                <div className="col-md-6">  

                    
                    <p className="close-change"><strong>{quote.close}</strong> {quote.change}  </p> 

                    <p className="cloed-on">Closed on: {quote.latestTime}</p>
                                    

                    <p className= "p-title"><strong>Sector:</strong></p>                    
                    <p>{quote.sector}</p>

                    <p className= "p-title"><strong>Primary Exchange:</strong></p> 
                    <p>{quote.primaryExchange}</p>  

                    <p className= "p-title"><strong>Previous Close:</strong></p> 
                    <p>{quote.previousClose}</p> 

                    <p className= "p-title"><strong>Market Percent:</strong></p>
                    <p>{quote.iexMarketPercent} %</p>
                    
                    <p className= "p-title"><strong>Shares Volume:</strong></p>
                    <p>{quote.latestVolume}</p>
                     


                    </div>

                    <div className="col-md-6">

                    <h4>Latest Variatons</h4>
                    
                    <p>Highest: {quote.high}</p>
                    <p>Lowest: {quote.low}</p>
                    <p>Open: {quote.open}</p>
                    <p>Close: {quote.close}</p>

                     <p className= "p-title"><strong>Avg Total Volume:</strong></p>
                    <p>{quote.avgTotalVolume}</p>

                    <button className="btn btn-secondary">
                     <Link to={`/`}>Voltar</Link> 
                    </button>  

                    </div>

                    </div>         

                </div>
            </div>
        );

    }
}

export default Quote;