import React, { Component } from 'react';
import axios from 'axios';

class Company extends Component {

    constructor(props) {
        super(props);

        this.state = {
            company: {},
        };
    }

    componentWillMount(){
        this.loadCompany();        
    }

    async loadCompany (){        
        const {symbol} = this.props;   
        const response = await axios.get(`https://api.iextrading.com/1.0/stock/${symbol}/company`);
        this.setState ( { company: response.data });
    }

    render() {
         const { company } = this.state;

        return ( 
            <div className="stock-box">        
                <div className="stock-header">                
                    <h3>{company.companyName} ({company.symbol})</h3> 
                    <div>
                        <p>{company.symbol}</p>
                        <p>{company.companyName}</p>
                        <p>{company.exchange}</p>
                        <p>{company.industry}</p>
                        <p>{company.website}</p>
                        <p>{company.description}</p>
                        <p>{company.issueType}</p>
                        <p>{company.sector}</p>                    
                    </div>
                </div>
            </div>
        );
    }
}

export default Company;