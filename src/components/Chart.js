import React, { Component } from 'react';
import axios from 'axios';

class Chart extends Component {

    constructor(props) {
        super(props);

        this.state = {
            chart: {},
        };
    }

    componentWillMount(){
        this.loadCharts();
    }


    async loadCharts (){        
        const {symbol} = this.props;   
        const response = await axios.get(`https://api.iextrading.com/1.0/stock/${symbol}/chart`);
        this.setState ( { chart: response.data });

    }

    render() {
         const { chart } = this.state;
        return ( 
        
            <div className="stock-box">        
                <div className="stock-header">                     


                <p>Some chart data</p>                    
                
                    
                </div>
            </div>
        );


        
    }
}

export default Chart;