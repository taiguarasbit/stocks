import _ from 'lodash'
import React, { Component } from 'react'
import { Search, Grid, Header, Segment } from 'semantic-ui-react'
import { Link } from 'react-router-dom';



export default class SearchExampleStandard extends Component {
  componentWillMount() {
    this.resetComponent()    
  }

  resetComponent = () => this.setState({ isLoading: false, results: [], value: '' })

  handleResultSelect = (e, { result }) => this.setState({ value: result.symbol })

  handleSearchChange = (e, { value }) => {
    this.setState({ isLoading: true, value })
    

    setTimeout(() => {
      if (this.state.value.length < 1) return this.resetComponent()

      const re = new RegExp(_.escapeRegExp(this.state.value), 'i')
      const isMatch = result => re.test(result.symbol)

      this.setState({
        isLoading: false,
        results: _.filter(this.props.stocks, isMatch),
      })
    }, 300)
  }

  render() {
    const { isLoading, value, results } = this.state

    const resRender = ({symbol}) => (
      <span key="name">
        <Link to={`/stocks/${symbol}`}>{symbol}</Link>
      </span>
          );

    return (
      <Grid>
        <Grid.Column width={12}>
          <Search
            loading={isLoading}
            onResultSelect={this.handleResultSelect}
            onSearchChange={_.debounce(this.handleSearchChange, 500, { leading: true })}
            results={results}
            resultRenderer={resRender}
            value={value}
            {...this.props}
          />
          
        </Grid.Column>

        <Grid.Column width={10}>
          <Segment>
            <Header>State</Header>
            <pre style={{ overflowX: 'auto' }}>{JSON.stringify(this.state, null, 2)}</pre>
            <Header>Options</Header>
            <pre style={{ overflowX: 'auto' }}>{JSON.stringify(this.props.stocks, null, 2)}</pre>
          </Segment>
        </Grid.Column>
      </Grid>
    )
  }
}
