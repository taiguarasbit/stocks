import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import Quote from '../Quote';
import { Link } from 'react-router-dom';


describe('Quote', () => {
  let mountedQuote;

  beforeEach(() => {
    mountedQuote = shallow(<Quote />);
  });

  it('renders without crashing', () => {
    shallow(<Quote />);
  });

  it('expect to have a back Link button', () => {
    expect(mountedQuote.find(Link)).to.have.lengthOf(1);
  });
});