import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import CustomSearch from '../CustomSearch';
import { Search } from 'semantic-ui-react';


describe('CustomSearch', () => {
  let mountedCustomSearch;

  beforeEach(() => {
    mountedCustomSearch = shallow(<CustomSearch />);
  });

  it('renders without crashing', () => {
    shallow(<CustomSearch />);
  });

  it('expect to have a UI Semantic Search component', () => {
    expect(mountedCustomSearch.find(Search)).to.have.lengthOf(1);
  });
});