import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import { Route } from "react-router-dom";
import Routes from '../Routes';


describe('Routes', () => {
  let mountedRoutes;

  beforeEach(() => {
    mountedRoutes = shallow(<Routes />);
  });

  it('renders without crashing', () => {
    shallow(<Routes />);
  });

  it('expect to have links to page', () => {
    expect(mountedRoutes.find(Route)).to.have.lengthOf(2);
  });
});