import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Layout from '../Layout';
import Navbar from '../../components/Navbar';
import Footer from '../../components/Footer';


describe('Layout Page', () => {
  let mountedLayout;

  beforeEach(() => {
    mountedLayout = shallow(<Layout />);
  });

  it('renders without crashing', () => {
    shallow(<Layout />);
  });

  it('expect to have Navbar component', () => {
    expect(mountedLayout.find(Navbar)).to.have.lengthOf(1);
  });

  it('expect to have Footer component', () => {
    expect(mountedLayout.find(Footer)).to.have.lengthOf(1);
  });
});