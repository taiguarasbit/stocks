import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import Navbar from '../Navbar';
import { Menu } from 'semantic-ui-react';


describe('Navbar', () => {
  let mountedNavbar;

  beforeEach(() => {
    mountedNavbar = shallow(<Navbar />);
  });

  it('renders without crashing', () => {
    shallow(<Navbar />);
  });

  it('expect to have Menu components', () => {
    expect(mountedNavbar.find(Menu)).to.have.lengthOf(1);
  });

  it('expect to have two Menu items components', () => {
    expect(mountedNavbar.find(Menu.Item)).to.have.lengthOf(2);
  });
});