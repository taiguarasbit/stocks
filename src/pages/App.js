import React, { Component } from 'react';
import Routes from '../components/Routes';
import styled from 'styled-components';

const Div = styled.div`
.intro-content{
  padding-top: 55px;
  text-align: center;
  display: flex;
  flex-flow:column;    
}

.ui.grid{
  text-align: center;
  justify-content: center;
  padding-top: 25px;
}
.twelve{
  padding:0;
}


.footer{
  text-align: center;
 }
`;

class App extends Component {
  render() {
    return (
      <Div>
        <Routes /> 
      </Div>   
    );
  }
}

export default App;