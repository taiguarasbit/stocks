import React, { Component } from 'react';
import Quote from '../components/Quote';
import Company from '../components/Company';
import Chart  from '../components/Chart';
import Layout from '../components/Layout';

class Detail extends Component {

    render() {

        const symbol = this.props.match.params.symbol;

        return (
            <Layout>
                <div className="intro-content"> 
                    <Quote symbol={symbol}/>
                    <Company symbol={symbol}/>
                    <Chart symbol={symbol}/>
                </div>
            </Layout>
        );
        
    }
}

export default Detail;