import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import App from '../App';
import Routes from '../../components/Routes';


describe('App Page', () => {
  let mountedApp;

  beforeEach(() => {
    mountedApp = shallow(<App />);
  });

  it('renders without crashing', () => {
    shallow(<App />);
  });

  it('expect to have router component', () => {
    expect(mountedApp.find(Routes)).to.have.lengthOf(1);
  });
});