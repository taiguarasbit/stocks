import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Detail from '../Detail';
import Chart from '../../components/Chart';
import Company from '../../components/Company';
import Quote from '../../components/Quote';
import Layout from '../../components/Layout';


describe('Detail', () => {
  let mountedDetail;

  beforeEach(() => {
    mountedDetail = shallow(<Detail />);
  });

  it('renders without crashing', () => {
    shallow(<Detail />);
  });

  it('expect to have Layout component', () => {
    expect(mountedDetail.find(Layout)).to.have.lengthOf(1);
  });

  it('expect to have Quote component', () => {
    expect(mountedDetail.find(Quote)).to.have.lengthOf(1);
  });

  it('expect to have Company component', () => {
    expect(mountedDetail.find(Company)).to.have.lengthOf(1);
  });

  it('expect to have Chart component', () => {
    expect(mountedDetail.find(Chart)).to.have.lengthOf(1);
  });
});