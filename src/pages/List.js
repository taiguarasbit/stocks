import React, { Component } from 'react';
import axios from 'axios';
import PlainSearch from '../components/PlainSearch';
import Layout from '../components/Layout';

class Intro extends Component {


    constructor(props) {
        super(props);
        this.state = {
          error: null,
          isLoaded: false,          
          stocks: []          
        };
      }

      componentDidMount() {   
        this.loadStocks();
      }


      loadStocks = async() => {
        const response = await axios.get('https://api.iextrading.com/1.0/tops');    
        this.setState({ stocks : response.data })        
        };


    render() {
        return (

            <Layout>
                <div className="intro-content">                
                    <PlainSearch stocks={this.state.stocks}/>
                </div>
            </Layout>



        );
    }


}


export default Intro
