import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ListStockSymbol } from '../redux/actions';
import Layout from '../components/Layout';
import CustomSearch from '../components/CustomSearch';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

class Main extends Component {

      render() {

        return(
            <Layout>
                <div className="main-page">
                    <div className="intro-content"> 
                        <div className="intro-content">
                            <button onClick={this.props.handleStocks}>Call apple stock click here</button>   
                            <Link to={`/stocks/aapl`}>Apple Detail page</Link>
                            <CustomSearch stocks={this.props.stocks } />                            
                        </div>
                    </div>
                </div>
            </Layout>
        );
        
    }
}

Main.propTypes = {
    handleStocks: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
    value: state.value,
    stocks: state.stocks
});

const mapDispatchToProps = (dispatch) => ({
   handleStocks: () => ListStockSymbol(dispatch, 'aapl')
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Main);