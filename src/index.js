import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './server/serviceWorker';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import rootreducer from './redux/reducers';
import App from './pages/App';

const store = createStore(rootreducer);

ReactDOM.render(  
<Provider store={store}>
    <App />
  </Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
