import axios from 'axios';  

const API_URL = 'https://api.iextrading.com/1.0/stock'; 

export async function ListStockSymbol(dispatch, symbol ) {  
    const response = await axios.get(`${API_URL}/${symbol}/company`);
    return dispatch( {
        type: 'FETCH_BY_SYMBOL',
        stocks: response.data
    });
}


//async loadStocks (){
//    const response = await axios.get('https://api.iextrading.com/1.0/tops');
 //   this.setState({ stocks : response.data });
//  }