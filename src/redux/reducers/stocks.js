const stocks = (state = [], action) => {
    switch (action.type) {
      case 'FETCH_BY_SYMBOL':
        return action.stocks;
      default:
        return {stocks:[]};
    }
  };
  
  export default stocks;