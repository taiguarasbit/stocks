import { expect } from 'chai';
import stocks from '../stocks';

describe("Reducers", ()=>{
  let action;

  beforeEach(() => {
    action = {
      type: 'FETCH_BY_SYMBOL',
      stocks:'appl'
    };
  });

  it("should return correct stock", () => {
    let expected = stocks({},action);
    expect(expected).to.equal(action.stocks);
  });
});


